# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

d = {}
exec(open("axon_velocity/version.py").read(), None, d)
version = d['version']
long_description = open("README.md").read()

entry_points = None

install_requires = []

setup(
    name="axon_velocity",
    version=version,
    author="Alessio Buccino, Xinyue Yuan",
    author_email="alessiop.buccino@gmail.com",
    description="Python package for axonal tracking form extracellular action potentials",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.bsse.ethz.ch/abuccino/axon_velocity",
    install_requires=[
        'numpy',
        'matplotlib',
        'scipy',
        'networkx',
        'sklearn',
        'MEAutility'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    packages=find_packages(),
    entry_points=entry_points,
    include_package_data=True,
)
