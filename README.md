# Axon velocity

Graph-based algorithm to compute track axonal branches and compute velocities from extracellular 
multi-electrode array (MEA) recordings in Python.

## Installation 

To install the `axon_velocity` package, you can clone the repo and install using pip:

```bash
git clone https://git.bsse.ethz.ch/abuccino/axon_velocity.git
cd axon_velocity
python setup.py install (or develop)
```

### Requirements

`axon_velocity` depends on the following packages, which are automatically installed

- numpy
- networkx
- matplotlib
- scipy
- MEAutility

For simulations:

1. Install NEURON from [here](https://www.neuron.yale.edu/neuron/download) - recommended version 7.7
2. Install LFPy: `pip install LFPy`
3. Install neuroplotlib (visualization): `pip install neuroplotlib`

## Usage

The inputs to the tracking algorithm are:

- templates: mean extracellular waveforms (n_channels x n_samples)
- locations: x-y position of electrodes (n_channels x 2)

The graph-based method can be run as follows:

```python
import axon_velocity as av

gtr = av.compute_graph_propagation_velocity(template=your_template, locations=your_locations)
```

To inspect available arguments, you can use `av.compute_graph_propagation_velocity?`. 

The output `gtr` is an object of a class called `GraphAxonTracking`. 
It contains the following fields:

- `branches`: List of dictionaries containing the following fields:
    - 'selected_channels': selected channels in the path
    - 'velocity': velocity estimate in mm/s (if locations in um and fs in Hz)
    - 'offset': offset of velocity estimate (same units as locations)
    - 'r2': r-squared of the velocoty fit
    - 'error': standard error of the linear fit
    - 'pval': p_value of the fit
    - 'distances': array with distances computed along the branch
    - 'peak_times': array with peak differences with initial channel
    - 'init_channel': channel used as initial channel
- `selected_channels`: List of selected channels
- `graph`: NetworkX directed graph 

The `GraphAxonTracking` also implements useful methods for plotting the selected channels 
(`gtr.plot_channel_selection()`), plot the underlying graph (`gtr.plot_graph()`), plot the selected axonal branches 
(`gtr.plot_branches()`), and plot the estimated velocities for each branch (`gtr.plot_velocities()`).
    
    
For comparison, or simpler cases, also a simple algorithm that only uses the peak difference and distance with respect
to the initial channel is implemented. It can be called similarly with:

```python
str = av.compute_simple_propagation_velocity(template=your_template, locations=your_locations)
```

## Contribute

Contributions are welcome! Before pushing, make sure to clean up all notebooks with `nbconvert`:

`pip install nbconvert` (just once)

`jupyter nbconvert --to notebook --ClearOutputPreprocessor.enabled=True --ClearMetadataPreprocessor.enabled=True  --inplace **/*.ipynb` (before committing) 